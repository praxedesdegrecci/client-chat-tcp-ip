﻿using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net.Sockets;

namespace Client_Eco_Chat
{
    public partial class Client : Form
    {

        //definindo variaveis
        TcpClient clienteSocket = new TcpClient();
        NetworkStream servidorStream = default(NetworkStream);
        string recebedata = null;
        int iporta;
        private String nome, ip, porta;


        public object Visibility { get; internal set; }

        //inicializando o FORM
        public Client(String name, String ip, String porta)
        {
            InitializeComponent();

            //Alterando configurações quando iniciar.
            //Puxando as variáveis para os campos digitaveis e
            //bloqueando eles ate que o chat seja desconectado

            nome = name;
            this.ip = ip;
            this.porta = porta;
            iporta = int.Parse(porta);

            conectar();

        }

        //evento ao pressionar a tecla limpa
        private void btn_clear_Click(object sender, EventArgs e)
        {
            txt_history.Text = "";
        }

        //evento ao apertar send 
        private void btn_send_Click(object sender, EventArgs e)
        {
            enviar();

        }

    


        //evento desconectar ( arrumando... )

        private void desconectar()
        {
            try
            {
                clienteSocket.GetStream().Close(); //fecha socket de conexao
                servidorStream.Close();
            }catch
            {
                Console.WriteLine("Erro ao fechar conexão.");
            }
            
        }

        // evento ao pressionar conectar
        private void conectar()
        {
            try // tratamento de erro ao tentar conectar/ inciar o cliente
            {
                iporta = int.Parse(porta); // convertendo ip string para int
                //readData = "Voce Conectou.";
                msg(); // chamando a função de mensagem
                clienteSocket.Connect(ip, iporta);
                servidorStream = clienteSocket.GetStream();
                byte[] outStream = Encoding.UTF8.GetBytes(nome + "$"); // convertendo em byte
                servidorStream.Write(outStream, 0, outStream.Length);
                servidorStream.Flush(); // limpando
                Thread ctThread = new Thread(getMessage);// instanciando thread para enviar e receber mensagem ao mesmo tempo
                ctThread.Start(); // iniciando
                txt_history.Text = "--------------------------------->>   Conexão estabelecida com o Server   <<---------------------------------";
            }
            catch
            {
                MessageBox.Show("Falha ao se conectar, verifique o IP e Porta ou entre em contato com o administrador do servidor!");
                this.Close();
            }
        }

        private void enviar() // funcao enviar
        {
            if (txt_send.Text != "") //diferente de vazio, envia
            {
                try
                {
                    byte[] outStream = System.Text.Encoding.UTF8.GetBytes(txt_send.Text + "$");
                    servidorStream.Write(outStream, 0, outStream.Length);
                    servidorStream.Flush(); //limpa "cache"
                    txt_send.Text = ""; //limpa campo após enviar
                }
                catch
                {
                    MessageBox.Show("Por favor ,verifique a sua conexão com o servidor!"); //caso os ervidor seja fechado ou de erro de conexao
                }
                
            }
        }

        private void getMessage() // funcao receber mensagem
        {
            while (true)
            {
                try
                {
                    servidorStream = clienteSocket.GetStream();
                    byte[] inStream = new byte[1024];
                    servidorStream.Read(inStream, 0, inStream.Length);
                    string returndata = System.Text.Encoding.UTF8.GetString(inStream);
                    recebedata = "" + returndata;
                    msg();
                }
                catch
                {
                }
            }
        }

        private void txt_history_KeyPress(object sender, KeyPressEventArgs e) // enviar mensagem com 'enter'
        {
            
            if (e.KeyChar == 13)
            {
                // Deabilita o campo para nao permitir prescionar o enter durante o processamento
                txt_send.Enabled = false;
                try
                {
                    // Executa ou faz chamada ao metodo q vc deseja
                    enviar();
                }
                catch
                {
                    throw;
                }
                finally
                {
                    // Reabilita o campo
                    txt_send.Enabled = true;
                    // Nao propaga o precionamento da tecla enter
                    e.Handled = true; 
                    // Da o foco no textbox para o ibeam voltar nele.
                    txt_send.Focus();
                }
            }

        }

       

        private void txt_history_TextChanged(object sender, EventArgs e) // caixa de texto do historico
        {
            txt_history.ScrollBars = ScrollBars.Vertical; // add scrollbar
            txt_history.WordWrap = true;
            txt_history.SelectionStart = txt_history.Text.Length; // scrollbar automatico
            txt_history.ScrollToCaret();
            txt_history.ReadOnly = true;// nao e possivel edicao nesta textbox, apenas leitura/copiar .
        }

        private void msg()
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(msg));
            else
                txt_history.Text = txt_history.Text + Environment.NewLine + " >> " + recebedata;

        }

        private void Client_FormClosing(object sender, FormClosingEventArgs e) // Finalizar app ao fechar, pois o Launcher fica oculto ao abrir o client.
        {
            desconectar();// desconectando
            Environment.Exit(0); //encerrando aplicação, pois o launcher fica em hide quando inciado o client.
        }
    }

}
