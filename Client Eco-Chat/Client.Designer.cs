﻿namespace Client_Eco_Chat
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Client));
            this.txt_send = new System.Windows.Forms.TextBox();
            this.txt_history = new System.Windows.Forms.TextBox();
            this.btn_send = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_send
            // 
            this.txt_send.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_send.Location = new System.Drawing.Point(12, 291);
            this.txt_send.Multiline = true;
            this.txt_send.Name = "txt_send";
            this.txt_send.Size = new System.Drawing.Size(349, 58);
            this.txt_send.TabIndex = 0;
            this.txt_send.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_history_KeyPress);
            // 
            // txt_history
            // 
            this.txt_history.BackColor = System.Drawing.Color.White;
            this.txt_history.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_history.Location = new System.Drawing.Point(12, 12);
            this.txt_history.Multiline = true;
            this.txt_history.Name = "txt_history";
            this.txt_history.Size = new System.Drawing.Size(438, 244);
            this.txt_history.TabIndex = 1;
            this.txt_history.TextChanged += new System.EventHandler(this.txt_history_TextChanged);
            this.txt_history.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_history_KeyPress);
            // 
            // btn_send
            // 
            this.btn_send.BackColor = System.Drawing.Color.YellowGreen;
            this.btn_send.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_send.FlatAppearance.BorderSize = 4;
            this.btn_send.Font = new System.Drawing.Font("Trebuchet MS", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_send.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_send.Location = new System.Drawing.Point(368, 291);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(82, 58);
            this.btn_send.TabIndex = 6;
            this.btn_send.Text = "→";
            this.btn_send.UseVisualStyleBackColor = false;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            this.btn_send.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_history_KeyPress);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(12, 262);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(75, 23);
            this.btn_clear.TabIndex = 7;
            this.btn_clear.Text = "Limpar";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(463, 361);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_send);
            this.Controls.Add(this.txt_history);
            this.Controls.Add(this.txt_send);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Client";
            this.Text = "Eco Chat";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Client_FormClosing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_history_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_send;
        private System.Windows.Forms.TextBox txt_history;
        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.Button btn_clear;
    }
}