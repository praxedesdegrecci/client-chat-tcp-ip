﻿using System;
using System.Windows.Forms;

namespace Client_Eco_Chat
{

    public partial class Launcher_Client : Form
    {
        private String name, ip, porta;

        public Launcher_Client()
        {
            
            InitializeComponent();
            txt_porta.Text = "666"; //definindo porta padrão do servidor
            txt_ip.Text = "";
            txt_name.Select();//CAMPO DIGITAVEL JÁ SELECIONADO inicialmente

        }


        //Inicializa o CHAT
        private void button1_Click(object sender, EventArgs e)
        {
            iniciar(); 
        }

                
        //mesmo evento que o INICIAR é acionado ou pressionar o ENTER
        private void tbJhonmps_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                iniciar();
            }
        }

        private void iniciar()
        {
            //Passa por argumentos as variaveis escolhida pelo usuario
            if (txt_ip.Text == "" || txt_porta.Text == "")
            {
                MessageBox.Show("Digite o Ip e a Porta");
            }
            if (txt_name.Text == "")
            {
                MessageBox.Show("Digite o nome de usuário");
            }
            else
            {
                name = txt_name.Text;
                ip = txt_ip.Text;
                porta = txt_porta.Text;

                Client clt = new Client(name, ip, porta); // instancia o novo form

                try
                {
                    clt.Show(); // exibe nova form
                    this.Hide(); // esconde form do launcher
                }
                catch
                {
                    clt.Close();// caso form de erro ,ele vai fechar.
                }


            }
        }


        }
}
